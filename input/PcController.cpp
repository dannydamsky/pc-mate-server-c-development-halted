#include "PcController.h"

ostream &operator<<(ostream &os, const PcController &controller) {
    os << "input: " << controller.input;
    return os;
}

PcController::PcController() = default;

PcController::~PcController() {
    delete this->input;
}

void PcController::send(const QString &event) {
    PcInput *pcInput = PcInput::getInputMode(event);
    if (pcInput == nullptr) {
        delete pcInput;
        if (this->input != nullptr) {
            this->input->apply(event);
        }
    } else {
        //delete this->input;
        this->input = pcInput;
    }
}
