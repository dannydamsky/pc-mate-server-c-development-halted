#pragma once

#ifndef BLUETOOTHSERVER_PCCONTROLLER_H
#define BLUETOOTHSERVER_PCCONTROLLER_H


#include <input/interface/PcInput.h>
#include <ostream>
#include <QString>

class PcController {
public:
    PcController();

    ~PcController();

    void send(const QString &event);

    friend ostream &operator<<(ostream &os, const PcController &controller);

private:
    PcInput *input;
};


#endif //BLUETOOTHSERVER_PCCONTROLLER_H
