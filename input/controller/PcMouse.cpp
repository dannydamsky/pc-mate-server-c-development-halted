#include "PcMouse.h"
#include <util/StringUtils.h>
#include <QCursor>
#include <QtCore/QStringList>

void PcMouse::apply(const QString &command) {
    if (command.size() <= 10) {
        const QStringList list = command.split(';', QString::SplitBehavior::SkipEmptyParts);

        const QString &xStr = list.at(0);
        const QString &yStr = list.at(1);
        bool okX, okY;
        const int dx = xStr.toInt(&okX, 10);
        const int dy = yStr.toInt(&okY, 10);

        if (okX && okY) {
            QPoint currPoint = QCursor::pos();
            const int newX = currPoint.x() + dx;
            const int newY = currPoint.y() + dy;
            QCursor::setPos(newX, newY);
        } else if (xStr == PcInput::ACTION_MOUSE_SCROLL) {

        } else if (yStr == PcInput::ACTION_MOUSE_SCROLL) {

        } else if (xStr == PcInput::ACTION_MOUSE_ZOOM_IN) {

        } else if (xStr == PcInput::ACTION_MOUSE_ZOOM_OUT) {

        } else if (xStr == PcInput::ACTION_MOUSE_LEFT) {

        } else if (xStr == PcInput::ACTION_MOUSE_RIGHT) {

        } else if (xStr == PcInput::ACTION_MOUSE_MIDDLE) {

        } else if (xStr == PcInput::ACTION_MOUSE_RELEASE) {

        } else {
            cout << "Error in PcMouse::Apply xStr=" << xStr.toStdString() << " yStr=" << yStr.toStdString() << endl;
        }
    }
}
