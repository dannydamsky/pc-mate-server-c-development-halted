#pragma once

#ifndef BLUETOOTHSERVER_PCMOUSE_H
#define BLUETOOTHSERVER_PCMOUSE_H

#include <input/interface/PcInput.h>

class PcMouse : public PcInput {
public:
    void apply(const QString &command) override;

private:
    const char *type = PcInput::IDENTIFIER_MOUSE;
};


#endif //BLUETOOTHSERVER_PCMOUSE_H
