#pragma once

#ifndef BLUETOOTHSERVER_PCINPUT_H
#define BLUETOOTHSERVER_PCINPUT_H

#include <ostream>
#include <string>
#include <QString>

using namespace std;

class PcInput {
public:
    static const char *IDENTIFIER_MOUSE;
    static const char *IDENTIFIER_KEYBOARD;
    static const char *ACTION_MOUSE_LEFT;
    static const char *ACTION_MOUSE_MIDDLE;
    static const char *ACTION_MOUSE_RIGHT;
    static const char *ACTION_MOUSE_RELEASE;
    static const char *ACTION_MOUSE_SCROLL;
    static const char *ACTION_MOUSE_ZOOM_IN;
    static const char *ACTION_MOUSE_ZOOM_OUT;

    static PcInput *getInputMode(const QString &event);

    PcInput() = default;

    virtual ~PcInput() = default;

    virtual void apply(const QString &event) = 0;

    friend std::ostream &operator<<(std::ostream &os, const PcInput &input) {
        os << input.type;
        return os;
    }

private:
    const char *type = nullptr;
};

#endif //BLUETOOTHSERVER_PCINPUT_H
