#include <input/controller/PcMouse.h>

const char *PcInput::IDENTIFIER_MOUSE = "%V9jDY5#m%";
const char *PcInput::IDENTIFIER_KEYBOARD = "v3&qB&uSS%";
const char *PcInput::ACTION_MOUSE_LEFT = "le";
const char *PcInput::ACTION_MOUSE_MIDDLE = "mi";
const char *PcInput::ACTION_MOUSE_RIGHT = "ri";
const char *PcInput::ACTION_MOUSE_RELEASE = "re";
const char *PcInput::ACTION_MOUSE_SCROLL = "sc";
const char *PcInput::ACTION_MOUSE_ZOOM_IN = "zi";
const char *PcInput::ACTION_MOUSE_ZOOM_OUT = "zo";

PcInput *PcInput::getInputMode(const QString &event) {
    PcInput *input;
    if (event == IDENTIFIER_MOUSE) {
        auto pcMouse = new PcMouse;
        input = pcMouse;
    } else {
        input = nullptr;
    }

    return input;
}
