cmake_minimum_required(VERSION 3.12)
project(BluetoothServer)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_AUTOMOC ON)
set(CMAKE_INCLUDE_CURRENT_DIR ON)

find_package(Qt5Core CONFIG REQUIRED)
find_package(Qt5Gui CONFIG REQUIRED)
find_package(Qt5Widgets CONFIG REQUIRED)
find_package(Qt5Bluetooth CONFIG REQUIRED)

set(SOURCE_FILES main.cpp
        server/InputServer.cpp
        server/InputServer.h
        server/connector/ServerConnector.cpp
        server/connector/ServerConnector.h
        input/PcController.cpp
        input/PcController.h
        input/interface/PcInput.h
        input/controller/PcMouse.cpp
        input/controller/PcMouse.h
        input/controller/PcKeyboard.cpp
        input/controller/PcKeyboard.h
        util/StringUtils.h input/interface/PcInput.cpp)

add_executable(BluetoothServer ${SOURCE_FILES})
target_link_libraries(BluetoothServer
        Qt5::Core
        Qt5::Gui
        Qt5::Widgets
        Qt5::Bluetooth
        )