#pragma once

#ifndef BLUETOOTHSERVER_SERVERCONNECTOR_H
#define BLUETOOTHSERVER_SERVERCONNECTOR_H

#include <qbluetoothserviceinfo.h>
#include <qbluetoothsocket.h>
#include <qbluetoothhostinfo.h>
#include <server/InputServer.h>
#include <input/PcController.h>

QT_USE_NAMESPACE

class ServerConnector : public QObject {
Q_OBJECT

public:
    explicit ServerConnector(QObject *parent = nullptr);

    ~ServerConnector() override;

private slots:

    void showMessage(const QString &sender, const QString &message);

    void clientConnected(const QString &name);

    void clientDisconnected(const QString &name);

private:
    InputServer *server;
    QBluetoothHostInfo localAdapter;
    PcController *controller;
};


#endif //BLUETOOTHSERVER_SERVERCONNECTOR_H
