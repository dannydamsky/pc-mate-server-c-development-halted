#include <QtBluetooth/QBluetoothLocalDevice>
#include "ServerConnector.h"

ServerConnector::ServerConnector(QObject *parent) : QObject(parent) {
    this->controller = new PcController;

    QList<QBluetoothHostInfo> localAdapters = QBluetoothLocalDevice::allDevices();
    if (localAdapters.count() >= 1) {
        this->localAdapter = localAdapters.at(0);
        QBluetoothLocalDevice adapter(this->localAdapter.address());
        adapter.setHostMode(QBluetoothLocalDevice::HostDiscoverable);

        this->server = new InputServer(this);
        connect(this->server, SIGNAL(clientConnected(QString)), this, SLOT(clientConnected(QString)));
        connect(this->server, SIGNAL(clientDisconnected(QString)), this, SLOT(clientDisconnected(QString)));
        connect(this->server, SIGNAL(messageReceived(QString, QString)), this, SLOT(showMessage(QString, QString)));
        this->server->startServer();
    }
}

ServerConnector::~ServerConnector() {
    delete this->server;
}

void ServerConnector::showMessage(const QString &sender, const QString &message) {
    std::cout << message.toStdString() << std::endl;
    this->controller->send(message);
}

void ServerConnector::clientConnected(const QString &name) {
    std::cout << name.toStdString() << std::endl;
}

void ServerConnector::clientDisconnected(const QString &name) {
    std::cout << name.toStdString() << std::endl;
}
