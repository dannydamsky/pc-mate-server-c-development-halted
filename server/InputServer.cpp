#include "InputServer.h"

#include <qbluetoothserver.h>

static const QLatin1String SERVICE_UUID("b35a16de-258b-4031-8a8c-0f51439abe59");
static const char *SERVICE_NAME = "Bluetooth Input Server";
static const char *SERVICE_DESC = "Bluetooth server used to for processing input events and translating them into input from mouse, keyboard, etc.";
static const char *SERVICE_PROVIDER = "Danny Damsky";

InputServer::InputServer(QObject *parent) : QObject(parent), rfcommServer(nullptr) {
}

InputServer::~InputServer() {
    stopServer();
}

void InputServer::startServer(const QBluetoothAddress &localAdapter) {
    if (rfcommServer)
        return;

    rfcommServer = new QBluetoothServer(QBluetoothServiceInfo::RfcommProtocol, this);
    connect(rfcommServer, SIGNAL(newConnection()), this, SLOT(clientConnected()));
    const bool result = rfcommServer->listen(localAdapter);
    if (!result) {
        qWarning() << "Cannot bind Bluetooth server to" << localAdapter.toString();
        return;
    }

    QBluetoothServiceInfo::Sequence classId;

    classId << QVariant::fromValue(QBluetoothUuid(QBluetoothUuid::SerialPort));
    serviceInfo.setAttribute(QBluetoothServiceInfo::BluetoothProfileDescriptorList, classId);

    classId.prepend(QVariant::fromValue(QBluetoothUuid(SERVICE_UUID)));

    serviceInfo.setAttribute(QBluetoothServiceInfo::ServiceClassIds, classId);

    serviceInfo.setAttribute(QBluetoothServiceInfo::ServiceName, tr(SERVICE_NAME));
    serviceInfo.setAttribute(QBluetoothServiceInfo::ServiceDescription, tr(SERVICE_DESC));
    serviceInfo.setAttribute(QBluetoothServiceInfo::ServiceProvider, SERVICE_PROVIDER);

    serviceInfo.setServiceUuid(QBluetoothUuid(SERVICE_UUID));

    QBluetoothServiceInfo::Sequence publicBrowse;
    publicBrowse << QVariant::fromValue(QBluetoothUuid(QBluetoothUuid::PublicBrowseGroup));
    serviceInfo.setAttribute(QBluetoothServiceInfo::BrowseGroupList, publicBrowse);

    QBluetoothServiceInfo::Sequence protocolDescriptorList;
    QBluetoothServiceInfo::Sequence protocol;
    protocol << QVariant::fromValue(QBluetoothUuid(QBluetoothUuid::L2cap));
    protocolDescriptorList.append(QVariant::fromValue(protocol));
    protocol.clear();
    protocol << QVariant::fromValue(QBluetoothUuid(QBluetoothUuid::Rfcomm))
             << QVariant::fromValue(quint8(rfcommServer->serverPort()));
    protocolDescriptorList.append(QVariant::fromValue(protocol));
    serviceInfo.setAttribute(QBluetoothServiceInfo::ProtocolDescriptorList,
                             protocolDescriptorList);

    serviceInfo.registerService(localAdapter);
}

void InputServer::stopServer() {
    serviceInfo.unregisterService();
    qDeleteAll(clientSockets);
    delete rfcommServer;
    rfcommServer = nullptr;
}

void InputServer::clientConnected() {
    QBluetoothSocket *socket = rfcommServer->nextPendingConnection();
    if (!socket)
        return;

    connect(socket, SIGNAL(readyRead()), this, SLOT(readSocket()));
    connect(socket, SIGNAL(disconnected()), this, SLOT(clientDisconnected()));
    clientSockets.append(socket);
    emit clientConnected(socket->peerName());
}

void InputServer::clientDisconnected() {
    auto *socket = qobject_cast<QBluetoothSocket *>(sender());
    if (!socket)
        return;

    emit clientDisconnected(socket->peerName());

    clientSockets.removeOne(socket);

    socket->deleteLater();
}

void InputServer::readSocket() {
    auto *socket = qobject_cast<QBluetoothSocket *>(sender());
    if (!socket)
        return;

    if (socket->isReadable()) {
        QByteArray str = socket->readAll();
        emit messageReceived(socket->peerName(),
                             QString::fromUtf8(str.constData(), str.length()));
    }
}
