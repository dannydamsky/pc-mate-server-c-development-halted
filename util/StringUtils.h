#pragma once

#ifndef BLUETOOTHSERVER_STRINGUTILS_H
#define BLUETOOTHSERVER_STRINGUTILS_H

#include <vector>
#include <string>
#include <iostream>
#include <algorithm>

using namespace std;

class StringUtils {
public:
    static vector<string> split(const string &s, const char &chr);

    static bool isNumber(const string &s);
};

vector<string> StringUtils::split(const string &s, const char &chr) {
    bool start = true;
    string part;
    vector<string> vector;
    vector.reserve(s.length() / 2);

    for (char c : s) {
        if (c == chr) {
            if (!start) {
                vector.push_back(part);
            } else {
                start = false;
            }
            part.clear();
        }
        part += c;
    }

    return vector;
}

bool StringUtils::isNumber(const string &s) {
    return !s.empty() && find_if(s.begin(), s.end(), [](char c) { return !std::isdigit(c); }) == s.end();
}

#endif //BLUETOOTHSERVER_STRINGUTILS_H
