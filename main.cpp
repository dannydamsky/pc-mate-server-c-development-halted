#include <iostream>
#include <QApplication>

using namespace std;

int main(int argc, char *argv[]) {
    QApplication app(argc, argv);

    ServerConnector serverConnector(&app);

    return QApplication::exec();
}